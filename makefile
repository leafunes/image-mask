BIN_DIR = ./bin
C_DIR = ./src/c
ASM_DIR = ./src/asm


debug: clean compile_debug
	gdb --args ./bin/executable $(img1) $(img2) $(mask) $(csv)

run : compile
	./bin/executable $(img1) $(img2) $(mask) $(csv)

compile : main.o
	gcc -m32 \
	$(BIN_DIR)/image_processor.o \
	$(BIN_DIR)/main.o \
	-o $(BIN_DIR)/executable

compile_debug: main.o_debug
	gcc -m32 \
	$(BIN_DIR)/image_processor.o \
	$(BIN_DIR)/main.o \
	-o $(BIN_DIR)/executable

main.o_debug : image_processor.o_debug
	gcc -m32 -c -g -o $(BIN_DIR)/main.o $(C_DIR)/main.c

image_processor.o_debug :
	nasm -f elf -F dwarf -g $(ASM_DIR)/image_processor.asm -o $(BIN_DIR)/image_processor.o

main.o : image_processor.o
	gcc -m32 -c -o $(BIN_DIR)/main.o $(C_DIR)/main.c

image_processor.o :
	nasm -f elf -F dwarf $(ASM_DIR)/image_processor.asm -o $(BIN_DIR)/image_processor.o


clean:
	rm -rf $(BIN_DIR)/*