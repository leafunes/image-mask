section .data
   ones_1 dq 0xffffffffffffffff
   ones_2 dq 0xffffffffffffffff

section .text
   global enmascarar_asm

%define buffer_a ebp+8
%define buffer_b ebp+12
%define buffer_mask ebp+16
%define len ebp+20
enmascarar_asm:
   enter 0,0
   push ebx
   ;asm code

   mov eax, [buffer_a]
   mov ebx, [buffer_b]
   mov ecx, [buffer_mask]
   ;mov edx, [ebp + 20] ;len
   mov esi, 0 ; contador
   movdqu xmm4, [ones_1]

   interlacing:
      movdqu xmm0, [eax + esi] ;buffer_a
      movdqu xmm1, [ebx + esi] ;buffer_b
      movdqu xmm2, [ecx + esi] ;buffer_mask
      pand xmm0, xmm2 ; and de la mascara
      pandn xmm2, xmm4 ; doy vuelta la mascara
      pand xmm1, xmm2 ; and de la not mascara
      paddusb xmm0, xmm1 ; sumo los resultados
      movdqu [eax + esi], xmm0 ; escribo en memoria
      add esi, 16
      cmp esi, [len]
      jb interlacing

      sub esi, 15 ; le resto 15, porque se que me pase. Es equivalente a restar 16 y sumar 1

   interlacing_bytes:
      mov edi, eax ;guardamos el puntero
      mov al, [edi + esi] ; byte del buffer a
      mov ah, [ebx + esi] ; bytes del buffer b
      mov dl, [ecx + esi] ; bytes de la mascara

      and al, dl
      not dl
      and ah, dl
      add al, ah
      mov byte[edi], al
      add esi, 1
      cmp esi, [len]
      jne interlacing_bytes

   quit:
   ;leave sequence
   pop ebx
   leave
   ret