#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern void enmascarar_asm(unsigned char* a, unsigned char* b, unsigned char* mask, int len);

typedef struct
{
    int width;
    int height;
    int len;
    unsigned char* pixels;

} PPMImage;


PPMImage* read_image(char* path){
    int x, y;
    PPMImage* img;
    FILE* fp = fopen(path, "r");
    if (!fp) {
              printf("No se pudo abrir la imagen %s\n", path);
              return NULL;
    }

    // primeros dos chars
    getc(fp);
    getc(fp);
    getc(fp);
    // Alto y ancho
    fscanf(fp, "%d %d", &x, &y);

    img = (PPMImage *)malloc(sizeof(PPMImage));
    img->height = x;
    img->width = y;
    img->len = x * y * 3;

    while (fgetc(fp) != '\n') ;
    while (fgetc(fp) != '\n') ;
    img->pixels = (unsigned char*)malloc(img->height * img->width * sizeof(unsigned char) * 3);
    fread(img->pixels, 3 * img->height, img->width, fp);

    fclose(fp);

    printf("%s leida, con tamaño %i, %i, cantidad de pixels %i y total de bytes %i \n", path, 
        x, y, img->height * img->width, img->height * img->width * 3);
    
    return img;
}

void write_csv_entry(char* path, char* line){
    FILE* fp = fopen(path, "a");
    fprintf(fp,"%s\n", line);
    fclose(fp);
}

int write_image(char* path, PPMImage *img){
    FILE* fp = fopen(path, "w+");
    fprintf(fp, "P6\n");

    fprintf(fp, "%d %d\n", img->height, img->width);
    fprintf(fp, "255\n");
    fwrite(img->pixels, sizeof(unsigned char), img->len, fp);
    
    fclose(fp);

    printf("%s escrita, con tamaño %i, %i, cantidad de pixels %i y total de bytes %i \n", path, 
        img->height, img->width, img->height * img->width, img->height * img->width * 3);
    
}

void enmascarar(unsigned char* a, unsigned char* b, unsigned char* mask, int len){

    for(int i = 0; i < len; i++){
        a[i] = mask[i] & a[i];
        a[i] += ~(mask[i]) & b[i];
    }

}

int main (int argc, char *argv[]){

    clock_t start, end;
    double cpu_time_used_asm, cpu_time_used_c;

    char* first_image = (char*)malloc(64 * sizeof(char));
    char* second_image = (char*)malloc(64 * sizeof(char));
    char* mask_image = (char*)malloc(64 * sizeof(char));
    char* csv_file = (char*)malloc(64 * sizeof(char));
    char* result_image = "img/result.ppm";
    char* result_image_asm = "img/result_asm.ppm";

    sprintf(first_image, "img/%s.ppm", argv[1]);
    sprintf(second_image, "img/%s.ppm", argv[2]);
    sprintf(mask_image, "img/%s.ppm", argv[3]);
    sprintf(csv_file, "%s", argv[4]);

    PPMImage* image_a = read_image(first_image);
    PPMImage* image_b = read_image(second_image);
    PPMImage* image_mask = read_image(mask_image);

    start = clock();
    enmascarar_asm(image_a->pixels, image_b->pixels, image_mask->pixels, image_mask->len);
    end = clock();
    cpu_time_used_asm = ((double) (end - start) * 1000 / CLOCKS_PER_SEC);

    write_image(result_image_asm, image_a);

    printf("%f\n", cpu_time_used_asm);
    printf("Se tardo en enmascarar con assembly %f milisegundos\n", cpu_time_used_asm);

    start = clock();
    enmascarar(image_a->pixels, image_b->pixels, image_mask->pixels, image_mask->len);
    end = clock();
    cpu_time_used_c = ((double) (end - start) * 1000 / CLOCKS_PER_SEC) ;

    write_image(result_image, image_a);

    printf("%f\n", cpu_time_used_c);
    printf("Se tardo en enmascarar con c %f milisegundos\n", cpu_time_used_c);
    
    char* line = (char*) malloc(sizeof(cpu_time_used_c) + sizeof(cpu_time_used_c) + 2);
    sprintf(line, "%f,%f", cpu_time_used_c,cpu_time_used_asm);
    write_csv_entry(csv_file, line);

    free(first_image);
    free(second_image);
    free(mask_image);
    free(csv_file);

    return 0;
}