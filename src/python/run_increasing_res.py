import argparse
import subprocess
from shutil import copy2
from PIL import Image

Image.MAX_IMAGE_PIXELS = None #Desabilito el control DOS Bomb de PIL

def get_args():
	parser = argparse.ArgumentParser(description='Repeated image mask runnings increasing image resolution.')
	parser.add_argument('img1', help = "Image 1 without path and extension")
	parser.add_argument('img2', help = "Image 2 without path and extension")
	parser.add_argument('mask', help = "Mask without path and extension")
	parser.add_argument('csv', help = "CSV to store c result")
	parser.add_argument('-i','--iterations', dest='iter', type=int, default=1,
	                    help='number of iterations')
	parser.add_argument('--factor-scale', dest='factor_scale', type=float,
						default=1.1, help='Image factor scale')

	return parser.parse_args()

def resize_image(path, escalate_factor):
	image = Image.open(path,'r')
	width, height = image.size
	width *= escalate_factor
	height *= escalate_factor

	r_image = image.resize((round(width), round(height)))

	print ("Widht: %f, height: %f" % r_image.size)

	r_image.save(path)

def main():
	args = get_args()
	
	copy2(args.img1, "../../img/img_1_aux.ppm")
	copy2(args.img2, "../../img/img_2_aux.ppm")
	copy2(args.mask, "../../img/mask_aux.ppm")

	for i in range(args.iter):

		cmd = "make run img1=%s img2=%s mask=%s csv=%s" % ("img_1_aux", "img_2_aux", "mask_aux" , args.csv)
		subprocess.run(cmd.split(),cwd='../../')

		resize_image("../../img/img_1_aux.ppm", args.factor_scale)
		resize_image("../../img/img_2_aux.ppm", args.factor_scale)
		resize_image("../../img/mask_aux.ppm", args.factor_scale)

if __name__ == '__main__':
	main()
