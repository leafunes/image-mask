from PIL import Image
import click

@click.command()
@click.argument('imgname')
@click.option('--inverse/--normal', '-i/-n', default=False)
def main(imgname, inverse):
    if not inverse:
        im = Image.open("../../img/" + imgname + ".png")
        im.save( "../../img/" + imgname + ".ppm")
    else:
        im = Image.open("../../img/" + imgname + ".ppm")
        im.save( "../../img/" + imgname + ".png")

if __name__ == "__main__":
    main()
