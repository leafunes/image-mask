# Tp para Organización del computador II - UNGS

## Alumnos

Lucas Addisi

Funes Leandro

## Descripción general
Este proyecto toma dos imagenes y les aplica una máscara para interpolar los pixeles de la primera imagen en la segunda.

## Dependencias
- Nasm
- Gcc
- Python 3
    - [PIL](https://pypi.org/project/Pillow/)
- Make
- GDB (Opt)

## Como correr el proyecto

### Conversión de imágenes png a ppm
Primero hay que convertir las imagenes con python. Para ello hay que correr el siguiente script:
``` bash
cd src/python
python3 img_converter.py [NOMBRE_DE_IMAGEN]
```

En dónde NOMBRE_DE_IMAGEN es el nombre de una imagen .png (pero sin el .png) alojada en el directorio ``img``.

Este comando convierte una imagen ``.png`` en una imagen ``.ppm``. El formato [ppm](http://netpbm.sourceforge.net/doc/ppm.html) es un formato simple en dónde la imagen se representa como un array de bytes con los valores RGB. 

Ejemplo:

```
P3
# feep.ppm
4 4
15
 0  0  0    0  0  0    0  0  0   15  0 15
 0  0  0    0 15  7    0  0  0    0  0  0
 0  0  0    0  0  0    0 15  7    0  0  0
15  0 15    0  0  0    0  0  0    0  0  0
```

Para convertir un ``png``, por ejemplo, si existe el archivo ``img/img_1.png``:
``` bash
cd src/python
python3 img_converter.py img_1
```

Luego de convertir la imagen, hay que correr el programa en C

### Aplicación de máscara
Para aplicar la mascara hay que correr el proyecto en C, para ello:

- Para compilar y correr el proyecto hay que hacer ``make run img1=NOMBRE_DE_PRIMERA_IMAGEN_PNG img2=NOMBRE_DE_SEGUNDA_IMAGEN_PNG mask=NOMBRE_DE_MASCARA_PNG csv=ARCHIVO_CSV``
- Para compilar el proyecto hay que hacer ``make compile ``
- Para debugear (con gdb) el proyecto hay que hacer ``make debug img1=NOMBRE_DE_PRIMERA_IMAGEN_PNG img2=NOMBRE_DE_SEGUNDA_IMAGEN_PNG mask=NOMBRE_DE_MASCARA_PNG csv=ARCHIVO_CSV``
- Para limpiar el proyecto hay que hacer ``make clean``

Al correr el proyecto, el ejecutable aplicará la mascara a la primara imagen sobre la segunda, y se generarán dos archivos: ``img/result.ppm`` e ``img/result_asm.ppm``. Además, el programa escribirá en ARCHIVO_CSV el tiempo de ejecución de la ejecución en C y la ejecución en Assembly en formate CSV

### Conversión de imágenes ppm a png
Luego de obtener los dos archivos de resultados hay que "desconvertir" los ``.ppm`` en ``.png``, para ello se ejecuta:

```bash
cd src/python
python3 img_converter -i [NOMBRE_DE_LA_IMAGEN]
```
La imagen resultante estará en ``img/[NOMBRE_DE_LA_IMAGEN].ppm``

## Arquitectura de la solución
La arquitectura general consta de tres partes:
- Conversor de imagen en python
  - Se encarga de convertir las imagenes de ``png`` a ``ppm`` y viceversa
- Aplicador de mascara en C
  - Recorre las dos imagenes y aplica la máscara
- Aplicador de mascara en ASM
  - Recorre las dos imagenes y aplica la máscara, pero lo hace con instrucciones [SSE](https://es.wikipedia.org/wiki/SSE)

### Solución en Python

La solución en python en la más simple. Simplemente se hace uso de la libreria PIL para convertir de un formato a otro, y de la librería [click](https://pypi.org/project/click/) para soportar argumentos

### Solución en C
El código en C se encarga de tres cosas:
- Parsear los parametros y abrir las imagenes
- Llamar a la función de aplicador de máscara definida en ASM
- Aplicar la máscara en C de manera lineal
  - Para hacerlo, simplemente se recorre la primera imagen, segunda imagen y la mascara con el mismo indice

``` c
a[i] = mask[i] & a[i];
a[i] += ~(mask[i]) & b[i];
```

### Solución en ASM
La solución en Assembly es relativamente simple, básicamente se hace lo mismo que en C, pero con dos salvedades:
- Se utilizan instrucciones SSE, lo que permite que se procesen de a 16 bytes (o 5.3 pixeles, cada pixel está compuesto de 3 bytes)
- La instrucción **NOT** no existe en SSE, por lo que se hizo un buffer con 16 bytes de ``0xFF``
  - Luego se le aplica ``pandn`` entre el buffer y la máscara, esto genera una máscara negada 
- Los últimos pixeles de la imagen se recorren secuencialmente


## Resultados
En la comparativa de las dos soluciones, se han encontrado grandes diferencias en lo que respecta a los tiempos de ejecución. Claramente, la implementación de assembly tiene una notable mejora.  Esta es de un 2000% aproximadamente en las resoluciones 1920x1080 y 8k, y un 4000% en la resolución de 800x600.

Además, se ha ejecutado un script para correr el programa 57 veces, incrementando la resolución de la imágen por un factor del 5% por cada iteración. A continuación se muestran los resultados:

![800x600](res/800x600.png)

![1920x1080](res/1920x1080.png)

![8k](res/8k.png)

![Todas las resoluciones](res/all_resolutions.png)

![Ejecución iterativo con factor de escalado del 5% por cada iteración](res/iterative_res_1,05.png)